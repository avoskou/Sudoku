#include <iostream>
#include <fstream>
#include <cstdio>
#include <cstring>
#include <cassert>
#include <ctime>
#include <cstdlib>
#include "sudoku.h"

using namespace std;

/* You are pre-supplied with the functions below. Add your own 
   function definitions to the end of this file. */

/* pre-supplied function to load a Sudoku board from a file */
void load_board(const char* filename, char board[9][9]) {

  cout << "Loading Sudoku board from file '" << filename << "'... ";

  ifstream in(filename);
  if (!in)
    cout << "Failed!" << endl;
  assert(in);

  char buffer[512];

  int row = 0;
  in.getline(buffer,512);
  while (in && row < 9) {
    for (int n=0; n<9; n++) {
      assert(buffer[n] == '.' || isdigit(buffer[n]));
      board[row][n] = buffer[n];
    }
    row++;
    in.getline(buffer,512);
  }

  cout << ((row == 9) ? "Success!" : "Failed!") << endl;
  assert(row == 9);
}

/* internal helper function */
void print_frame(int row) {
  if (!(row % 3))
    cout << "  +===========+===========+===========+" << endl;
  else
    cout << "  +---+---+---+---+---+---+---+---+---+" << endl;
}

/* internal helper function */
void print_row(const char* data, int row) {
  cout << (char) ('A' + row) << " ";
  for (int i=0; i<9; i++) {
    cout << ( (i % 3) ? ':' : '|' ) << " ";
    cout << ( (data[i]=='.') ? ' ' : data[i]) << " ";
  }
  cout << "|" << endl;
}

/* pre-supplied function to display a Sudoku board */
void display_board(const char board[9][9]) {
  cout << "    ";
  for (int r=0; r<9; r++) 
    cout << (char) ('1'+r) << "   ";
  cout << endl;
  for (int r=0; r<9; r++) {
    print_frame(r);
    print_row(board[r],r);
  }
  print_frame(9);
}

/* add your functions here */


//************Task1**************************************************************


/* Checks if the sudoku board is completed*/

bool is_complete(char board[9][9]){
    for(int i=0;i<9;i++){
      for(int j=0;j<9;j++){
	if(board[i][j]=='.') return 0;
      }
    }
    return 1;
}




//******************************TASK2********************************************



/*Helper function - pass  position  coordinates from  string form  to  2 integers ( column and row) 8*/
bool  position_reader(const char position[],int &row,int &col)
{     if((int)position[0]<65 || (int)position[0]>73 ||(int)position[1]<49 || (int)position[1]>57) return 0; // return false for not valid coordinates
      row=((int)position[0])%65;
      col=((int)position[1])%49;
      return 1;
}


/*Helper function - checks the validation of input digit to a position given as 2 integers*/
bool digit_test(const int row,const int col,const char digit,char  board[9][9])
{    
     int ltl_square_col=col/3 ,ltl_square_row=row/3;  ;                    // row and col coordinate(0-2) of the minor  3x3   square  
     for(int i=0;i<9;i++) if(digit==board[i][col]) return 0;  
     for(int i=0;i<9;i++) if(digit==board[row][i]) return 0;
     for(int i=0;i<=2;i++)
       for(int j=0;j<=2;j++)
	 if(digit==board[ltl_square_row*3+i][ltl_square_col*3+j]) return 0; 
     return 1;
}     



/* pass to a cell the value of the digit ,if it is possible, and returns True else False.
 The coordinates of the cell are  given as a string */    
bool make_move(const char position[],const char digit,char board[9][9])
{ int col,row;
  if( !position_reader(position,row,col)) return 0; //checking if position is valid
  if(board[row][col]!='.' ||  !digit_test(row,col,digit,board))
     return 0;
  board[row][col]=digit;
  return 1;
}     




//******Task 3*******************************************************************



bool  save_board(const char* filename, char board[9][9])
{
  ofstream out;
  out.open(filename); 
  if (!out) return 0;                            // checks if the file opened properly
  for(int i=0;i<9;i++){
    for(int j=0;j<9;j++)
        out.put(board[i][j]);
    out<<endl;
  } 
 
  out.close();
  return 1;
}


//**** Taske 4*********************************************************************



/* Helper function - Pass to a cell the value of the digit ,if it is possible, and returns True else False.
     (It do not check if the target is empty)*/    
bool make_move_2(const int row,const int  col,const char digit,char board[9][9])
{
  if( !digit_test(row,col,digit,board)) return 0;
     board[row][col]=digit;
     return 1;
}   
 

/* Helper-  changes row and coll  to the cordinates of the first  empty cell*/
void  front_step(char board[9][9],int& row,int& col)
{ if(board[row][col]=='.') return ;
  while(row<=8){
    while(col<=8){
       if(board[row][col]=='.')  return   ;
       col++;
    }
    row++;
    col=0;
  }
  return ;
}

/* solve sudoku with backtracking*/
bool solve_board(char board[9][9])
{
  int col=0,row=0;
  front_step(board,row,col);
  for(char digit='1';digit<='9';digit++)   // tries to give a value to cell - if it is impossible to continue try an other value
    if(make_move_2(row,col,digit,board)){ 
       solve_board(board)  ;       // if it possible call itself an go to the next cell
       if( is_complete(board))  return 1;  // if it is complete return 0 else 
    }                               
  board[row][col]='.';
  return 0;
}  


//***********************Task 5******************************************************

/*Finds the Clock time needed for a board to be solved-return 0 if unsolvable*/	
int  clock_test(char board[9][9])
{
  clock_t begin = clock();                         
  if(!solve_board(board)) return 0 ;              
  clock_t end = clock();
  return  (end-begin);
  
}
  

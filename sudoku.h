// loading a sudoku
void load_board(const char* filename, char board[9][9]);

// displaying sudoku board
void display_board(const char board[9][9]);

// check if sudoku is completed
bool is_complete(char board[9][9]);

//tries to add a new number
bool make_move(const char position[],char digit,char  board[9][9]);

// save the sudoku board to txt
bool  save_board(const char* filename, char board[9][9]);

// front layer sudoku solver
bool  solve_board(char board[9][9]);

// finds the clock time needed to solve a sudoku
int  clock_test(char board[9][9]); 